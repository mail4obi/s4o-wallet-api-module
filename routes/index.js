const fs = require('fs');
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

const Config = require('../config');

const auth = async (req, res, next) => {
  try {
    let auth = req.headers.authorization;
    let parts = auth.split(' ');
    var decoded = jwt.verify(parts[1], Config.APP_SECRET);
    console.log('decoded jwt =>', decoded);
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
}

module.exports = {
  init: (app) => {
    app.use(cors());

    app.options("/*", cors());
    
    app.use(auth);

    app.use(bodyParser.urlencoded({
      extended: true
    }));

    app.use(bodyParser.json());

    let files = fs.readdirSync(__dirname);
    for (let file of files) {
      const name = file.split('.')[0];
      if (['index'].indexOf(name) < 0) {
        console.log('bootstrapping', name, 'route');
        const handle = require(`./${name}`);
        if (handle && typeof handle === 'function') handle(app);
      }
    }
  }
}
