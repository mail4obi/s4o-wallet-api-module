const Mapper = require('../routesMapper');

module.exports = (app) => {
  app.post('/initialize', initialize);
}

async function initialize(req, res) {
  try {
    let response = await Mapper.invoke('initialize', req.body);
    res.json(response);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}
