const Mapper = require('../routesMapper');

module.exports = (app) => {
  app.get('/accounts/balance', getBalance);
  app.get('/accounts/address', getAddress);
  app.post('/accounts/send', send);
}

async function getBalance(req, res) {
  try {
    let response = await Mapper.invoke('getBalance', req.query);
    res.json(response);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}

async function getAddress(req, res) {
  try {
    let response = await Mapper.invoke('getNewAddress', req.query);
    res.json(response);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}

async function send(req, res) {
  try {
    let response = await Mapper.invoke('sendMoney', { accountData: req.query, data: req.body });
    res.json(response);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}
