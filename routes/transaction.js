const Mapper = require('../routesMapper');

module.exports = (app) => {
  app.get('/transactions/:id', getTransaction);
  app.get('/transactions', getTransactions);
}

async function getTransaction(req, res) {
  try {
    let response = await Mapper.invoke('getTransaction', { hash: req.params.id, ...req.query });
    res.json(response);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}

async function getTransactions(req, res) {
  try {
    let response = await Mapper.invoke('getTransactions', req.query);
    res.json(response);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}
