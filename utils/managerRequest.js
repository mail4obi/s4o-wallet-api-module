const axios = require('axios');
const { get } = require('lodash');

const Config = require('../config');

module.exports = {
  rpc: async (method, params) => {
    try {
      let response = await axios({
        method: 'POST',
        url: Config.MANAGER_URL,
        data: { jsonrpc: '2.0', id: Date.now(), method, params },
        headers: {
          authorization: `Bearer ${Config.MANAGER_TOKEN}`
        }
      });
      if (response.data.error) {
        console.log(response.data);
        throw new Error(response.data.error.message);
      }
      return response.data.result;
    } catch (error) {
      throw new Error(get(error, 'message', 'unable to get manager response'));
    }
  }
}
