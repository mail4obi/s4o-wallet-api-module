module.exports = {
  availabilityUpdateTask: [
    { rule: '20 * * * * *', action: 'task', invokeImmediate: true }
  ]
}
