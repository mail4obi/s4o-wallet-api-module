const jwt = require('jsonwebtoken');

const Config = require('../config');
const Request = require('../utils/managerRequest').rpc;

module.exports = {
  task: async () => {
    try {
      let currencies = Config.SUPPORTED_CURRENCIES.split(',').map((c) => c.trim());
      let payload = {
        URI: Config.URI,
        currencies,
        token: jwt.sign({ name: Config.NODE_NAME }, Config.APP_SECRET, { expiresIn: 120 })
      }
      console.log(payload);
      let response = await Request('remoteAvailability', payload);
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  }
}
