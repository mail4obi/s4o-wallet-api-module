module.exports = class Log {
  /**
   * 
   * @param {Log} data 
   */
  constructor(data) {
    this.key = '';
    this.type = '';
    this.level = '';
    this.data = '';

    Object.assign(this, data);
  }
}
