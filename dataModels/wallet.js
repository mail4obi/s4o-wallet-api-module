module.exports = class Wallet {
  /**
   * 
   * @param {Wallet} data 
   */
  constructor(data) {
    this.account = '';
    this.currency = '';
    this.balance = '';
    this.lastCheckedBlock = '';
    this.lastCheckedTransaction = '';
    this.lastCheckedTransactionIndex = '';
    this.lastUpdate = '';

    Object.assign(this, data);
  }
}
