module.exports = class Notification {
  /**
   * 
   * @param {Notification} data 
   */
  constructor(data) {
    this.txId = ''
    this.account = '';
    this.address = '';
    this.currency = '';
    this.amount = '';
    this.type = '';

    Object.assign(this, data);
  }
}
