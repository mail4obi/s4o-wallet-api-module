module.exports = class Address {
  /**
   * 
   * @param {Address} data 
   */
  constructor(data) {
    this.account = '';
    this.type = '';
    this.currency = '';
    this.label = '';
    this.address = '';

    Object.assign(this, data);
  }
}
