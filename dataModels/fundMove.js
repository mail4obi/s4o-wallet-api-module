module.exports = class FundMove {
  /**
   * 
   * @param {FundMove} data 
   */
  constructor(data) {
    this.txId = '';
    this.moveId = '';
    this.account = '';
    this.currency = '';
    this.amount = '';
    this.status = '';
    this.lastTry = '';

    Object.assign(this, data);
  }
}
