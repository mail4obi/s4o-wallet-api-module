## installation
```sh
npm install --save git+https://gitlab.com/mail4obi/s4o-wallet-api-module.git
```

## configs
- to enable availability update set TASK_LIST='availabilityUpdateTask' in the environment
- other configs needed can be found in the config.js file

## Data Access Layer
Find Models in 'dataModels' folder
The DAL communicates with the upstream s4o-coinbase-api endpoint
```js
/**
 * {
 *  address,
 *  lock,
 *  wallet,
 *  notification,
 *  log
 * }
 * */
const DAL = require('s4o-wallet-api-module/data');
```

## example integration
```js
const WalletModule = require('s4o-wallet-api-module');

/**
 * query (accountData) => { 
 *  id: string, 
 *  name: string, 
 *  test: boolean, 
 *  notificationUrl: string
 * }
 * */
WalletModule.Mapper.set('initialize', async (query) => {
  console.log(query);
  return {
    multiSend: false
  };
});

/**
 * query (accountData) => { 
 *  id: string, 
 *  name: string, 
 *  test: boolean, 
 *  notificationUrl: string
 * }
 * */
WalletModule.Mapper.set('getBalance', async (query) => {
  console.log(query);
  // handle request or throw not implemented
  throw new Error('method not implemented');
});

/**
 * query (accountData+) => { 
 *  id: string, 
 *  name: string, 
 *  test: boolean, 
 *  notificationUrl: string,
 *  label: string
 * }
 * */
WalletModule.Mapper.set('getNewAddress', async (query) => {
  console.log(query);
  // handle request or throw not implemented
  throw new Error('method not implemented');
});

/**
 * query (accountData+sendData) => { 
 *  accountData: {
 *    id: string, 
 *    name: string, 
 *    test: boolean, 
 *    notificationUrl: string
 *  },
 *  data: {
 *    data: {
 *      '<destination_address1>': <amount>,
 *      '<destination_address2>': <amount>,
 *       ...
 *    },
 *    comment: string,
 *    fee: number
 *  }
 * }
 * */
WalletModule.Mapper.set('sendMoney', async (query) => {
  console.log(query);
  // handle request or throw not implemented
  throw new Error('method not implemented');
});

/**
 * query (accountData+) => { 
 *  id: string, 
 *  name: string, 
 *  test: boolean, 
 *  notificationUrl: string,
 *  hash: string
 * }
 * */
WalletModule.Mapper.set('getTransaction', async (query) => {
  console.log(query);
  // handle request or throw not implemented
  throw new Error('method not implemented');
});

/**
 * query (accountData+) => { 
 *  id: string, 
 *  name: string, 
 *  test: boolean, 
 *  notificationUrl: string,
 *  count: number,
 *  skip: number
 * }
 * */
WalletModule.Mapper.set('getTransactions', async (query) => {
  console.log(query);
  // handle request or throw not implemented
  throw new Error('method not implemented');
});

const PORT = process.env.PORT || 9000;
const HOST = process.env.HOST || '127.0.0.1';
WalletModule.start(PORT, HOST);
```
