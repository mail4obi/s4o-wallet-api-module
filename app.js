const express = require('express');

const app = express();

require('./routes').init(app);

module.exports = app;
