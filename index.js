const backgroundJobHandler = require('background_job_handler');

const DAL = require('./data');
const app = require('./app');
const Mapper = require('./routesMapper');

module.exports = {
  app,
  DAL,
  Mapper,
  start: (PORT, HOST) => {
    app.listen(PORT, HOST, () => {
      console.log('server running on', HOST, PORT);
      backgroundJobHandler.start(`${__dirname}`);
    });
  }
}
