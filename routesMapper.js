const mapper = {};

module.exports = {
  set: (command, handler) => {
    mapper[command] = handler;
  },
  invoke: async (command, args) => {
    let handler = mapper[command];
    if (handler) {
      return await handler(args);
    } else {
      throw new Error('command not supported');
    }
  }
}
