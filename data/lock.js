const Redis = require('ioredis');
const UUID = require('uuid/v1');

const Config = require('../config');

const localRedis = new Redis(Config.LOCAL_REDIS_SERVER);

const getLockKey = () => {
  return UUID();
}

const acquireLock = async (key, value, time) => {  
  if (await localRedis.setnx(key, value)) {
    await localRedis.expire(key, time);
    return true;
  } 

  let ttl = await localRedis.ttl(key);
  console.log(key, ttl, time);
  if (ttl === -1) {
    await localRedis.set(key, value);
    await localRedis.expire(key, time);
    return true;
  }

  return false;
}

const releaseLock = async (key, value) => {
  let lockValue = await localRedis.get(key);

  if (lockValue === value) {
    await localRedis.del(key);
    return true;
  } else {
    return false;
  }
}

module.exports = {
  getLockKey,
  acquireLock,
  releaseLock
}
