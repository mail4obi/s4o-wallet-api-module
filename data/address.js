/* eslint-disable no-unused-vars */
const Request = require('../utils/managerRequest').rpc;
const Address = require('../dataModels/address');

module.exports = {
  getAddress,
  getAddresses,
  createAddress,
  updateAddress
}

/**
 * 
 * @param {String} id 
 * @returns {Promise<Address>}
 */
async function getAddress(id) {
  try {
    return await Request('getAddress', [id]);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Address} query
 */
async function getAddresses(query) {
  try {
    return await Request('getAddresses', query);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Address} data 
 * @returns {Promise<Address>}
 */
async function createAddress(data) {
  try {
    return await Request('createAddress', data);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Address} data 
 * @returns {Promise<Address>}
 */
async function updateAddress(query, data) {
  try {
    return await Request('updateAddress', { query, data });
  } catch (error) {
    throw new Error(error.message);
  }
}
