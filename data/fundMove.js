/* eslint-disable no-unused-vars */
const Request = require('../utils/managerRequest').rpc;
const FundMove = require('../dataModels/fundMove');

module.exports = {
  getFundMove,
  getFundMoves,
  createFundMove,
  updateFundMove
}

/**
 * 
 * @param {String} id 
 * @returns {Promise<FundMove>}
 */
async function getFundMove(id) {
  try {
    return await Request('getFundMove', [id]);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {FundMove} query
 */
async function getFundMoves(query) {
  try {
    return await Request('getFundMoves', query);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {FundMove} data 
 * @returns {Promise<FundMove>}
 */
async function createFundMove(data) {
  try {
    return await Request('createFundMove', data);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {FundMove} data 
 * @returns {Promise<FundMove>}
 */
async function updateFundMove(query, data) {
  try {
    return await Request('updateFundMove', { query, data });
  } catch (error) {
    throw new Error(error.message);
  }
}
