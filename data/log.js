/* eslint-disable no-unused-vars */
const Request = require('../utils/managerRequest').rpc;
const Log = require('../dataModels/log');

module.exports = {
  getLog,
  getLogs,
  createLog,
  updateLog
}

/**
 * 
 * @param {String} id 
 * @returns {Promise<Log>}
 */
async function getLog(id) {
  try {
    return await Request('getLog', [id]);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Log} query
 */
async function getLogs(query) {
  try {
    return await Request('getLogs', query);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Log} data 
 * @returns {Promise<Log>}
 */
async function createLog(data) {
  try {
    return await Request('createLog', data);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Log} data 
 * @returns {Promise<Log>}
 */
async function updateLog(query, data) {
  try {
    return await Request('updateLog', { query, data });
  } catch (error) {
    throw new Error(error.message);
  }
}
