/* eslint-disable no-unused-vars */
const Request = require('../utils/managerRequest').rpc;
const Wallet = require('../dataModels/wallet');

module.exports = {
  getWallet,
  getWallets,
  createWallet,
  updateWallet
}

/**
 * 
 * @param {String} id 
 * @returns {Promise<Wallet>}
 */
async function getWallet(id) {
  try {
    return await Request('getWallet', [id]);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Wallet} query
 */
async function getWallets(query) {
  try {
    return await Request('getWallets', query);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Wallet} data 
 * @returns {Promise<Wallet>}
 */
async function createWallet(data) {
  try {
    return await Request('createWallet', data);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Wallet} data 
 * @returns {Promise<Wallet>}
 */
async function updateWallet(query, data) {
  try {
    return await Request('updateWallet', { query, data });
  } catch (error) {
    throw new Error(error.message);
  }
}
