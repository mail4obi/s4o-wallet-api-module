/* eslint-disable no-unused-vars */
const Request = require('../utils/managerRequest').rpc;
const Notification = require('../dataModels/notification');

module.exports = {
  getNotification,
  getNotifications,
  createNotification,
  updateNotification
}

/**
 * 
 * @param {String} id 
 * @returns {Promise<Notification>}
 */
async function getNotification(id) {
  try {
    return await Request('getNotification', [id]);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Notification} query
 */
async function getNotifications(query) {
  try {
    return await Request('getNotifications', query);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Notification} data 
 * @returns {Promise<Notification>}
 */
async function createNotification(data) {
  try {
    return await Request('createNotification', data);
  } catch (error) {
    throw new Error(error.message);
  }
}

/**
 * 
 * @param {Notification} data 
 * @returns {Promise<Notification>}
 */
async function updateNotification(query, data) {
  try {
    return await Request('updateNotification', { query, data });
  } catch (error) {
    throw new Error(error.message);
  }
}
